import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Test from './Test';

configure({ adapter: new Adapter() });

test('show Hello World! text', () => {
  const wrapper = shallow(<Test />);
  expect(wrapper.text()).toEqual('Just a root level component');
});
